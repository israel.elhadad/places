import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:places/Widgets/ImageInput.dart';
import 'dart:io';

import 'package:places/models/place.dart';
import 'package:places/providers/greatPlace.dart';
import 'package:provider/provider.dart';
import 'package:places/Widgets/location_input.dart';



class addPlace extends StatefulWidget {
  static const routName = "/addPlaceScreen";
  @override
  _addPlaceState createState() => _addPlaceState();
}

class _addPlaceState extends State<addPlace> {
  final _titleController = TextEditingController();
  File _pickedImage;
  placeLocation _pickedLocation;

  void _selectImage(File pickedImage ){
    _pickedImage = pickedImage;
  }
  void _selectLocation(double let,double long ){
    _pickedLocation = placeLocation(latitube: let, longtube: long);
  }
  void _saveplace(){
    if(_titleController.text.isEmpty || _pickedImage == null|| _pickedLocation == null){
      return;
    }
    place newPlace = place(id: DateTime.now().toString(),title: _titleController.text,location:_pickedLocation ,image:_pickedImage);
    Provider.of<greatPlace>(context,listen: false).addPlace(newPlace);
    Navigator.of(context, rootNavigator: true).pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("add place"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Expanded(
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    children:<Widget> [
                      TextField(
                        decoration: InputDecoration(labelText: "TITLE",),
                        controller: _titleController,
                      ),
                      SizedBox(height: 10.0),
                      ImageInput(_selectImage),
                      SizedBox(height: 10),
                      locationInput(_selectLocation)
                    ],

          ),
                ),)),
          // ignore: deprecated_member_use
          RaisedButton.icon(
              onPressed: _saveplace,
              icon: Icon(Icons.add),
              label: Text("Add place"),
              elevation: 0,
              materialTapTargetSize:MaterialTapTargetSize.shrinkWrap ,
          color:Theme.of(context).accentColor,),


        ],
      ),
    );
  }
}
