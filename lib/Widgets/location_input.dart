import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:flutter/material.dart';
import 'package:places/Helpers/location_helper.dart';
import 'package:places/screens/MapScreen.dart';

class locationInput extends StatefulWidget {
  final Function selectLocation;
  locationInput(this.selectLocation);
  @override
  _locationInputState createState() => _locationInputState();
}

class _locationInputState extends State<locationInput> {
  String urlImage;

  void showpreview(double let,double long){
    String myUrlImage = LocationHelper.ganerateLocationPreviewImage(lutitube: let,longitube: long);
    setState(() {
      urlImage = myUrlImage;
    });

  }

  Future<void> _getLocationData() async{
    try{
      final locData = await Location().getLocation();
      showpreview(locData.latitude,locData.longitude);
      widget.selectLocation(locData.latitude,locData.longitude);
    } catch(error){
      return;
    }

  }
  Future <void>_onSelectLocation() async{
    final LatLng mySelectLocation = await Navigator.of(context).push(MaterialPageRoute(fullscreenDialog:true,builder: (ctx)=> MapScreen(isSelecting: true)));
    if(mySelectLocation == null){
      return;
    }
    showpreview(mySelectLocation.latitude,mySelectLocation.longitude);
    widget.selectLocation(mySelectLocation.latitude,mySelectLocation.longitude);

  }
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          alignment: Alignment.center,
          height: 170,
          width: double.infinity,
          decoration: BoxDecoration(border:Border.all(width: 1,color: Colors.grey)),
          child: urlImage == null?
               Text("NOT LOCATION",textAlign: TextAlign.center):
               Image.network(urlImage,fit: BoxFit.cover,width: double.infinity,)
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            // ignore: deprecated_member_use
            FlatButton.icon(
              icon:Icon(Icons.location_on),
              label: Text('corrent location'),
              textColor: Theme.of(context).primaryColor,
              onPressed:_getLocationData,
                ),
            // ignore: deprecated_member_use
            FlatButton.icon(
              icon:Icon(Icons.map),
              label: Text('select location'),
              textColor: Theme.of(context).primaryColor,
              onPressed: _onSelectLocation,
            )
          ],
        )
      ],
    );
  }
}
