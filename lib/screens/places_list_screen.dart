import 'package:flutter/material.dart';
import 'package:places/providers/greatPlace.dart';
import 'package:places/screens/placeDeatliceScreen.dart';
import 'package:provider/provider.dart';
import 'add_place_screen.dart';

class placesListScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("your places"),
        actions:< Widget >[
          IconButton(icon: Icon(Icons.add),
          onPressed: (){
            Navigator.of(context).pushNamed(addPlace.routName);
            },)
        ],

      ),
      body: FutureBuilder(
        future: Provider.of<greatPlace>(context,listen: false).fetchAndSetData(),
        builder: (ctx, snapshot)=>
        snapshot.connectionState == ConnectionState.waiting ?
        Center(child: CircularProgressIndicator())
        : Consumer<greatPlace>(
          child: Center(child: Text("not places, pleas add places")),
          builder:(ctx,graetPlaces,ch)=> graetPlaces.items.length <=0
              ? ch:
          ListView.builder(itemCount: graetPlaces.items.length,itemBuilder:(ctx ,index)=>ListTile(
            leading:CircleAvatar(
              backgroundImage: FileImage(
                  graetPlaces.items[index].image
              ),
            ),
            title: Text(graetPlaces.items[index].title),
            subtitle: Text(graetPlaces.items[index].location.adress),
            onTap:(){
              Navigator.of(context).pushNamed(placeDeatliceScreen.routName,arguments: graetPlaces.items[index].id);
              },
          ) )
        ),
      )

    );
  }
}