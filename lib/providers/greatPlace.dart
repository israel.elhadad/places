import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:places/models/place.dart';
import 'package:places/Helpers/db_helper.dart';
import 'package:places/Helpers/location_helper.dart';
import 'dart:io';
class greatPlace with ChangeNotifier {
  List <place> _items = [];

  List <place> get items{
    return _items;
  }
   Future <void> addPlace(place myplace)async{
    String adress = await LocationHelper.getPlaceAdress(myplace.location.latitube,myplace.location.longtube);
    final upgredLocation = placeLocation(latitube: myplace.location.latitube, longtube: myplace.location.longtube,adress: adress);
    myplace.location = upgredLocation;
    _items.add(myplace);
    notifyListeners();
    DBhelper.insert('user_places',{'id':myplace.id,'title': myplace.title,'image':myplace.image.path,'loc_lat':myplace.location.latitube,'loc_long':myplace.location.longtube ,'adress':myplace.location.adress});
  }

  Future<void> fetchAndSetData() async{
    final dataList = await DBhelper.getData('user_places');
    _items = dataList.map((item) => place(
     id: item['id'],
     title: item['title'],
     image: File(item['image']),
     location: placeLocation(latitube: item['loc_lat'],longtube: item['loc_long'],adress: item['adress'],)
   ) ).toList();

    notifyListeners();
  }

  place FindById(String id){
    return _items.firstWhere((element) =>element.id==id);
  }

}
