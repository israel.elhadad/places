import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:places/screens/MapScreen.dart';
import 'package:provider/provider.dart';
import 'package:places/providers/greatPlace.dart';

class placeDeatliceScreen extends StatelessWidget {
  static const routName = "\placeDetaile";
  @override
  Widget build(BuildContext context) {
    final id = ModalRoute.of(context).settings.arguments;
    final selectedPlace = Provider.of<greatPlace>(context,listen: false).FindById(id);
    return Scaffold(
      appBar: AppBar(
        title: Text(
          selectedPlace.title,

        ),
      ),body: Column(
      children:<Widget> [
        Container(
          height: 250,
          width: double.infinity,
          child: Image.file(
            selectedPlace.image,fit: BoxFit.cover,width: double.infinity,
          ),
        ),SizedBox(height: 10,),
        Text(
            selectedPlace.location.adress,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Colors.grey,
          fontSize: 20
        ),),
        SizedBox(height: 10,),
        // ignore: deprecated_member_use
        FlatButton(onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(fullscreenDialog:true,builder: (ctx)=> MapScreen(initinalLocation: selectedPlace.location,isSelecting: false,)));
        }, child: Text('View on map'))
      ],
    ),
    );
  }
}
