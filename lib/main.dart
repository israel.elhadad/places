import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:places/screens/add_place_screen.dart';
import 'package:places/screens/placeDeatliceScreen.dart';
import 'package:places/screens/places_list_screen.dart';
import 'package:provider/provider.dart';
import 'package:places/providers/greatPlace.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value:greatPlace() ,
      child: MaterialApp(
        title: 'great places',
        theme: ThemeData(

          primarySwatch: Colors.indigo,
          accentColor: Colors.amber
        ),
        home: placesListScreen(),
        routes: {
          addPlace.routName : (context)=> addPlace(),
          placeDeatliceScreen.routName: (context)=> placeDeatliceScreen()
        },
      ),
    );
  }
}



