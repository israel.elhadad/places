import 'dart:ffi';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter/widgets.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart' as syspaths;
import 'package:path/path.dart' as path;
class ImageInput extends StatefulWidget {
  final Function onSelectImage;
  ImageInput(this.onSelectImage);
  @override
  _ImageInputState createState() => _ImageInputState();
}

class _ImageInputState extends State<ImageInput> {
  File _storyImage;



  Future<void> _takePicture()async{
    final picker = ImagePicker();
    final imageFile = await picker.getImage(source: ImageSource.camera,maxWidth: 600);
    if (imageFile == null ){
      return;
    }
    setState(() {
      _storyImage = File(imageFile.path);
    });
    final appDir = await syspaths.getApplicationDocumentsDirectory();
    final filName = await path.basename(imageFile.path);
    final savedImage = await  File(imageFile.path).copy('${appDir.path}/$filName');
    if(savedImage == null){
      return;
    }
    widget.onSelectImage(savedImage);
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 150,
          height: 100,
          decoration: BoxDecoration(border: Border.all(width: 1,color: Colors.grey),),
          child: _storyImage !=null ?
          Image.file(
            _storyImage,
            fit: BoxFit.cover,
            width: double.infinity,) :
          Text("no story image"),
          alignment: Alignment.center,
        ),
        SizedBox(width: 10.0,),
        Expanded(child: FlatButton.icon(
          icon: Icon(Icons.camera),
          label: Text("Take Picture",textAlign: TextAlign.center,),
          onPressed: _takePicture,
        ),
        )
      ],
    );
  }
}
