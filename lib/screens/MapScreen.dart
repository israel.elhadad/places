import 'package:flutter/material.dart';
import 'package:places/models/place.dart';
import 'dart:async';
import 'package:google_maps_flutter/google_maps_flutter.dart';
class MapScreen extends StatefulWidget {

  final  placeLocation  initinalLocation;
  final  bool isSelecting;

  MapScreen({this.initinalLocation = const placeLocation(latitube: 31.7900197,longtube: 35.1912052), this.isSelecting = false});
  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  LatLng pickedPosition;

  setpositionMarker(LatLng myPosition){
   setState(() {
     pickedPosition = myPosition;
   });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(appBar: AppBar(
      title: Text("MY MAP"),
      actions: <Widget>[
        if(widget.isSelecting)
          IconButton(icon:Icon(Icons.cached), onPressed: pickedPosition == null?null:(){
            Navigator.of(context).pop(pickedPosition);
          })

      ],

    ),
    body: GoogleMap(
    mapType: MapType.hybrid,
    initialCameraPosition: CameraPosition(
    target:LatLng(
    widget.initinalLocation.latitube,
    widget.initinalLocation.longtube,
    ),
      zoom: 16,
    ),
      onTap: widget.isSelecting ? setpositionMarker  :null,
      markers: (pickedPosition == null && widget.isSelecting) ? null:{
      Marker(markerId:MarkerId("m1") ,position: pickedPosition ?? LatLng(
          widget.initinalLocation.latitube,widget.initinalLocation.longtube
      ),
      ),
      },
    ));
  }
}
